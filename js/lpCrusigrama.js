/*! lpCrusigrama v2.0.0 | (c) 2015 jQuery Edilson Laverde Molina license FREE*/
typeCross = "none";
numCross = 0;
function mayus(e) {
    e.value = e.value.toUpperCase();
}
jQuery.lpCusigrama = function(args) {
    settings = jQuery.fn.extend({
        id: "#lp",
        heigh: 0,
        width: 0,
        n_words: 0,
        review: "words",
        background_focus: "#F25C05",
        background_focusout: "#05AFF2",
        color_correct: "#FFF",
        color_incorrect: "#FFF",
        color: "#000",
        class: ".lp",
        position_Button_info:["-15px","-10px"],
        onWinner: function() {},
        onGoodNumber: function(num) {},
        onKeyup: function() {},
        onFocus: function(id, id_question) {},
        onFocusOut: function(id, id_question) {}
    }, args);
    var LP_heigh = settings[0].heigh;
    var LP_width = settings[0].width;
    //creacion tablero//
    tablero = new MultiDimensionalArray(LP_heigh + 1, LP_width + 1);
    //inicializacion del tablero todos vacios//
    for (i = 1; i < LP_heigh; i++) {
        for (e = 1; e < LP_width; e++) {
            tablero[i][e] = "none";
        }
    }
    //creacion metadatos y matriz  leras//
    dataWords = MultiDimensionalArray(14, 4);
    for (i = 1; i < settings[0].n_words; i++) {
        dataWords[i - 1][0] = settings[i].position_x;
        dataWords[i - 1][1] = settings[i].position_y;
        dataWords[i - 1][2] = settings[i].word.length;
        dataWords[i - 1][3] = settings[i].type;
        if (settings[i].type == 'horizontal') {
            $.words_horizontal(settings[i].word, settings[i].position_y, settings[i].position_x);
        } else {
            $.words_vertical(settings[i].word, settings[i].position_y, settings[i].position_x);
        }
    }
    //creacion grafica tablero//
    var tabla = '<table    border="0" cellpadding="0" cellspacing="0" >';
    for (i = 1; i < LP_heigh; i++) {
        tabla += '<tr>';
        for (e = 1; e < LP_width; e++) {
            if ((tablero[i][e]) == 'none') {
                tabla += '<td class="crusigram-td" id="t-' + i + '-' + e + '" >';
                tabla += '</td>';
            } else {

                tabla += '<td class="crusigram-td"  id="t-' + i + '-' + e + '" >';
              // tabla += '<input onkeyup="mayus(this);" data-value="' + tablero[i][e] + '" id="e-' + i + '-' + e + '" maxlength="1" placeholder="' + tablero[i][e] + '" type="text">';
               tabla += '<input onkeyup="mayus(this);" data-value="' + tablero[i][e] + '" id="e-' + i + '-' + e + '" maxlength="1"  type="text">';
                tabla += '</td>';
            }
        }
        tabla += '</tr>';
    }
    tabla += '<table>';
    $(settings[0].id_game).html(tabla);
    //eventos datas inputs//
    html1 = "<h1>Verticale</h1>";
    html2 = "<h1>Horizontales</h1>";
    for (i = 1; i < settings[0].n_words; i++) {
        if (settings[i].type == 'horizontal') {
            $.words_horizontal_dates(settings[i].word, settings[i].position_y, settings[i].position_x, settings[i].n_quetion);
        } else {
            $.words_vertical_dates(settings[i].word, settings[i].position_y, settings[i].position_x, settings[i].n_quetion);
        }
        $("#t-" + settings[i].position_y + "-" + settings[i].position_x).append("<span title='" + settings[i].question + "' id='num-" + settings[i].n_quetion + "' class='number' style='position: absolute;top:"+ settings[i].position_Button_info[0]+";left:"+ settings[i].position_Button_info[1]+";'>" + settings[i].n_quetion + "</span>");
        if (settings[0].id_questions != false) {
            if (settings[i].type == "vertical") {
                html1 += "<div class='questions' id='" + settings[i].id_question + "'>" + settings[i].question + "</div>";
            } else {
                html2 += "<div class='questions' id='" + settings[i].id_question + "'>" + settings[i].question + "</div>";
            }
        }
    }
    $(settings[0].id_questions).html(html1 + html2);
    $(".number").css("cursor","help");
    $("input").css("cursor","pointer");
};
function MultiDimensionalArray(iRows, iCols) {
    var i, j;
    var table = new Array(iRows);
    for (i = 0; i < iRows; i++) {
        table[i] = new Array(iCols);
        for (j = 0; j < iCols; j++) {
            table[i][j] = "";
        }
    }
    return (table);
}
jQuery.words_horizontal = function(word, fila, columna) {
    var letras = word.split("");
    var aux = 0;
    for (e = 0; e < letras.length; e++) {
        tablero[fila][columna + aux] = letras[e];
        aux += 1;
    }
}
jQuery.words_vertical = function(word, fila, columna) {
    var letras = word.split("");
    var aux = 0;
    for (e = 0; e < letras.length; e++) {
        tablero[fila + aux][columna] = letras[e];
        aux += 1;
    }
}
jQuery.words_horizontal_dates = function(word, fila, columna, i) {
    var letras = word.split("");
    var aux = 0;
    var ultimo = eval(letras.length) - eval(1);
    for (e = 0; e < letras.length; e++) {
        var line = eval(columna) + eval(aux);
        $("#e-" + fila + '-' + line).addClass("group" + i);
        $("#e-" + fila + '-' + line).data('type1', 'horizontal');
        if (ultimo == e) {
            $("#e-" + fila + '-' + line).data('final', 'true');
        }
        aux += 1;
    }
    $.hoverWords(i);
}
jQuery.words_vertical_dates = function(word, fila, columna, i) {
    var letras = word.split("");
    var aux = 0;
    var ultimo = eval(letras.length) - eval(1);
    for (e = 0; e < letras.length; e++) {
        var line = eval(fila) + eval(aux);
        $("#e-" + line + '-' + columna).addClass("group" + i);
        $("#e-" + line + '-' + columna).data('type2', 'vertical');
        if (ultimo == e) {
            $("#e-" + line + '-' + columna).data('final', 'true');
        }
        aux += 1;
    }
    $.hoverWords(i);
}
jQuery.hoverWords = function(i) {
    $(".group" + i).css("background", settings[0].background_focusout);
    $(".group" + i).css("color", settings[0].color);
    $(".group" + i).focus(function() {
        settings[0].onFocus($(this).data("hover"), settings[i].id_question);
        $(".group" + i).css("background", settings[0].background_focus);
    });
    $(".group" + i).focusout(function() {
        $(".group" + i).css("background", settings[0].background_focusout);
        settings[0].onFocusOut($(this).data("hover"), settings[i].id_question);
    });
    $(".group" + i).css("outline", "0px")
    $(".group" + i).keydown(function(e) {
        key = e.keyCode || e.which;
        //delete salto// 
        if (key == 8) {
            if ($(this).is('[readonly]')) {
                direcion='-';
                $.focused($(this).data("type1"),$(this).data("type2"),direcion,$(this).attr("id"));
                return false;
            }
        }
        //flechas ↑ ↓ → ←  der=39 izq=37 arri=38 aba=40 //
        if (key == 39 || key == 37 || key == 38 || key == 40){
        if(key==39){$.selectFocus($(this).attr("id"), "horizontal", "+")};    
        if(key==37){$.selectFocus($(this).attr("id"), "horizontal", "-")}; 
        if(key==40){$.selectFocus($(this).attr("id"), "vertical", "+")};    
        if(key==38){$.selectFocus($(this).attr("id"), "vertical", "-")};     
        return false;
        }
    });
    $(".group" + i).keyup(function(e) {
        settings[0].onKeyup();
        $(this).data("hover", i);
        key = e.keyCode || e.which;
        if (key == 13 || key == 46 || key == 39 || key == 37 || key == 38 || key == 40) {} else {
  
            var direcion = '+';
            if (key == 8) {
                if ($(this).is('[readonly]')) {return false;}
                direcion = '-'
            } else {
                direcion = '+'
            };
            $.focused($(this).data("type1"),$(this).data("type2"),direcion,$(this).attr("id"));
            if (settings[0].review == "words") {
                for (var h = 1; h <= settings[0].n_words; h++)
                    $.validate_words(".group" + h);
            }
            if (settings[0].review == "letters") {
                $.validate_letters(".group" + i);
            }
            $.validate_game();
        }
    });
}
jQuery.selectFocus = function(id, cross, direcion) {
    var str = id.split("-");
    if (cross == "horizontal") {
        numCross = eval(str[2]);
        if (direcion == '+') {numCross += 1} else {numCross -= 1};
        setTimeout(function() {
            $("#" + str[0] + '-' + str[1] + '-' + numCross).focus();
            $("#" + str[0] + '-' + str[1] + '-' + numCross).select();
        }, 10);
    }
    if (cross == "vertical") {
        numCross = eval(str[1]);
        if (direcion == '+') {numCross += 1} else {numCross -= 1};
        setTimeout(function() {
            $("#" + str[0] + '-' + numCross + '-' + str[2]).focus();
            $("#" + str[0] + '-' + numCross + '-' + str[2]).select();
        }, 10);
    }
}
jQuery.validate_words = function(clase) {
    var num1 = 0,num2 = 0,num3 = 0;
    $(clase).each(function(index) {
        num1 += 1;
        if ($(this).val() == $(this).data("value")) {num2 += 1;}
        if ($(this).val() != "") {num3 += 1;}
    });
    if (num1 == num2) {
        $(clase).css("color", settings[0].color_correct);
        $(clase).attr("readonly", true);
    } else {
        if (num1 == num3) {$(clase).css("color", settings[0].color_incorrect);} 
        else {
            $(clase).each(function(index) {
                if ($(this).is('[readonly]')) {} else {
                    $(this).css("color", settings[0].color);
                }

            });
        }
    }
}
jQuery.validate_letters = function(clase) {
    $(clase).each(function(index) {
        if ($(this).val() != "") {
            if ($(this).val() == $(this).data("value")) {
                $(clase).css("color", settings[0].color_correct);
            } else {
                $(clase).css("color", settings[0].color_incorrect);
            }
        }
    });
}
jQuery.focused=function(type1,type2,direcion,id){
            if (type1 == "horizontal") {
                if (type2 == "vertical" && typeCross == 2) {
                    $.selectFocus(id, "vertical", direcion);
                    typeCross = 2;
                }else {
                    $.selectFocus(id, "horizontal", direcion);
                    typeCross = 1;
                }
            }
            if (type2 == "vertical") {
                if (type1 == "horizontal" && typeCross == 1) {
                    $.selectFocus(id, "horizontal", direcion);
                    typeCross = 1;
                }else {
                    $.selectFocus(id, "vertical", direcion);
                    typeCross = 2;
                }
            }
}
var goods=0;
jQuery.validate_game=function(){
    var win=true;
    var termino=true;
    goods=0;
    for(var i=1; i<settings[0].n_words; i++){
        var numItems = $(".group"+i).length;
        var good=0;
        $(".group"+i).each(function(index) {
            if ($(this).val() == "") {
                termino=false;
            }
            if ($(this).val() != $(this).data("value")) {
                    win=false;
            }else{
                good+=1
            };
        });
        if(numItems==good){goods+=1;}
    }
    if(termino){
        $.goodCroos();
    }
    if(win){settings[0].onWinner();} 
}
jQuery.goodCroos=function(){
    console.log("goods"+goods);
    settings[0].onGoodNumber(goods);
    return goods;
}