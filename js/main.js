var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Guia General",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [{
    url: "sonidos/click.mp3",
    name: "clic"
}];
function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            t.events();
            t.setGame();
            t.animations();
            //precarga audios//
            ivo.load_audio(audios, onComplete = function () {
                ivo(ST + "preload").hide();
                stage1.play();
            });
        },
        methods: {
            setGame:function(){
                jQuery.lpCusigrama([{
                    heigh:23,
                    width:22,
                    background_focus:"#01a89e",
                    background_focusout:"#79be00",
                    color_correct: "#FFF",
                    color_incorrect: "#D5494D",
                    color: "#000",
                    review:"words",
                    n_words:10,
                    id_game:'#Stage_app',
                    id_questions:"#preguntas",
                    onFocus: function(id,id_question){
                        $("#"+id_question).css("background","#8C96C3");   
                    },
                    onFocusOut: function(id,id_question){
                        $("#"+id_question).css("background","transparent");
                    },
                    onWinner: function() {
                        
                    },
                    onKeyup: function() {
                        
                    },
                    onGoodNumber: function(num) {
                        var scorm= new UdeCScorm(false);
                        console.log("Nombre: " + scorm.info_user().name + " Id: " + scorm.info_user().id + " Nota: " + (num*5.5));
                        console.log("Nota: " +num);
                        if(num==9){
                            scorm.set_score(50);
                            stageWinner.play();
                        }
                        if(num==8 || num==7){
                            scorm.set_score(40);
                            stageWinner.play();
                        }
                        if(num==6){
                            scorm.set_score(30);
                            stageWinner.play();
                        }
                        if(num<=5){
                            scorm.set_score(20)
                            stageFail.play();
                        }
                    }
                },
                {
                    type:'vertical',
                    word:'RESILIENCIA',
                    id_question:'Stage_h1',
                    position_y:1,
                    position_x:4,
                    direction:"+",
                    question:"Habilidad que debe poseer todos los miembros de una organización frente a las vicisitudes o adversidades que se afrontan",
                    n_quetion:1,
                    position_Button_info:["-18px","3px"]
                },  
                {
                    type:'vertical',
                    word:'DINAMICO',
                    id_question:'Stage_h1',
                    position_y:7,
                    position_x:1,
                    direction:"+",
                    question:"Un liderazgo que conduzca a la acción permite implementar las estrategias",
                    n_quetion:6,
                    position_Button_info:["-18px","3px"]
                },  
                {
                    type:'vertical',
                    word:'ADAPTACION',
                    id_question:'Stage_h1',
                    position_y:6,
                    position_x:8,
                    direction:"+",
                    question:"Es una cualidad propia para gestionar el cambio o la innovación",
                    n_quetion:5,
                    position_Button_info:["-18px","3px"]
                },  
                {
                    type:'horizontal',
                    word:'GERENCIA',
                    id_question:'Stage_h1',
                    position_y:2,
                    position_x:3,
                    direction:"+",
                    question:"Traducción al castellano de la palabra management",
                    n_quetion:2,
                    position_Button_info:["3px","-18px"]
                },  
                {
                    type:'horizontal',
                    word:'DIRIGIR',
                    id_question:'Stage_h1',
                    position_y:4,
                    position_x:3,
                    direction:"+",
                    question:"Etimológicamente el término liderazgo traduce esta palabra",
                    n_quetion:3,
                    position_Button_info:["3px","-18px"]
                },  
                {
                    type:'horizontal',
                    word:'LIDERAZGO',
                    id_question:'Stage_h1',
                    position_y:6,
                    position_x:3,
                    direction:"+",
                    question:"Dimensión que busca influir en los demás colaboradores de la organización y cuyo concepto depende de unos enfoques",
                    n_quetion:4,
                    position_Button_info:["3px","-18px"]
                },
                {
                    type:'horizontal',
                    word:'INNOVACION',
                    id_question:'Stage_h1',
                    position_y:8,
                    position_x:3,
                    direction:"+",
                    question:"Habilidad que permite al líder y los colaboradores desarrollar procesos o productos nuevos",
                    n_quetion:7,
                    position_Button_info:["3px","-18px"]
                },
                {
                    type:'horizontal',
                    word:'LEAD',
                    id_question:'Stage_h1',
                    position_y:11,
                    position_x:6,
                    direction:"+",
                    question:"Es la raíz etimológica de donde proviene el término liderazgo",
                    n_quetion:8,
                    position_Button_info:["3px","-18px"]
                },
                {
                    type:'horizontal',
                    word:'COMPROMISO',
                    id_question:'Stage_h1',
                    position_y:13,
                    position_x:1,
                    direction:"+",
                    question:"Un líder debe poseer esta cualidad frente a sus colaboradores",
                    n_quetion:9,
                    position_Button_info:["3px","-18px"]
                } 
                ]);

            },
            events: function () {
                var t = this;
                ivo(ST + "stage1").on("click", function () {
                    ivo.play("clic");
                    stage1.stop();
                    stage1.timeScale(3).reverse();
                    stage2.play();
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
                ivo(ST + "stage2").on("click", function () {
                    ivo.play("clic");
                    stage2.stop();
                    stage2.timeScale(3).reverse();
                    stage3.play();
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
            },
            animations: function () {
                stage1 = new TimelineMax();
                stage1.append(TweenMax.from(ST + "stage1", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.staggerFrom(".triangilos", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.append(TweenMax.from(ST + "title", .8, {x: 1300, opacity: 0}), 0);
                stage1.stop();
                
                stage2 = new TimelineMax();
                stage2.append(TweenMax.from(ST + "stage2", .8, {x: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.staggerFrom(".example", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage2.stop();
                
                stage3 = new TimelineMax();
                stage3.append(TweenMax.from(ST + "stage3", .8, {x: 1300, opacity: 0}), 0);
                stage3.append(TweenMax.staggerFrom(".example", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage3.stop();
                
                stageFail = new TimelineMax();
                stageFail.append(TweenMax.from(ST + "stage-fail", .8, {x: 1300, opacity: 0}), 0);
                stageFail.append(TweenMax.staggerFrom(".example", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stageFail.stop();
                
                stageWinner = new TimelineMax();
                stageWinner.append(TweenMax.from(ST + "stage-good", .8, {x: 1300, opacity: 0}), 0);
                stageWinner.append(TweenMax.staggerFrom(".example", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stageWinner.stop();
            }
        }
    });
}