/***********************
* Acciones de composición de Adobe Edge Animate
*
* Editar este archivo con precaución, teniendo cuidado de conservar 
* las firmas de función y los comentarios que comienzan con "Edge" para mantener la 
* capacidad de interactuar con estas acciones en Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // los alias más comunes para las clases de Edge

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      
      Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {
         // introducir aquí código que se debe ejecutar cuando la composición está totalmente cargada
         yepnope({
            nope: [
               "css/udec.css",
               "css/ui-udec.css",
               "css/lpCrusigrama.css",
               "js/sweetalert.min.js",
               "js/jquery-3.3.1.min.js",
               "js/ivo.js",
               "js/TweenMax.min.js",
               "js/SCORM_API_wrapper.js",
               "js/UdeCScorm.js",
               "js/jquery-2.1.1.js",
               "js/lpCrusigrama.js",
               "js/howler.min.js",
               "js/main.js",
            ],
            complete: init,
         }); // end of yepnope
         function init() {
            main(sym);
         }
      });
    //Edge binding end
})("stage");
   //Edge symbol end:'stage'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-247297");
