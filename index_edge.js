/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['0', '0', '1046', '646', 'auto', 'auto'],
                            c: [
                            {
                                id: 'img-1',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"img-1.jpg",'0px','0px']
                            },
                            {
                                id: 'form-2',
                                type: 'image',
                                rect: ['675px', '336px', '351px', '87px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"form-2.png",'0px','0px'],
                                userClass: "triangilos"
                            },
                            {
                                id: 'form-3',
                                type: 'image',
                                rect: ['0px', '272px', '1046px', '369px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"form-3.png",'0px','0px'],
                                userClass: "triangilos"
                            },
                            {
                                id: 'form-1',
                                type: 'image',
                                rect: ['573px', '379px', '458px', '267px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"form-1.png",'0px','0px'],
                                userClass: "triangilos"
                            },
                            {
                                id: 'title',
                                type: 'group',
                                rect: ['117', '423', '558', '155', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'ico-1',
                                    type: 'image',
                                    rect: ['0px', '0px', '137px', '155px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"ico-1.png",'0px','0px']
                                },
                                {
                                    id: 'txt-1',
                                    type: 'image',
                                    rect: ['153px', '10px', '405px', '134px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"txt-1.png",'0px','0px']
                                }]
                            }]
                        },
                        {
                            id: 'stage2',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-azul',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-azul.jpg",'0px','0px']
                            },
                            {
                                id: 'img-2',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"img-2.png",'0px','0px']
                            },
                            {
                                id: 'ico-2',
                                type: 'image',
                                rect: ['110px', '204px', '134px', '137px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"ico-2.png",'0px','0px']
                            },
                            {
                                id: 't1',
                                type: 'text',
                                rect: ['274px', '205px', '461px', '134px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"color: rgb(255, 255, 255);\">¡A continuación, le invitamos a identificar la importancia del liderazgo en su área de trabajo, a través del desarrollo de la siguientes actividades!</span></p><p style=\"margin: 0px;\">​</p>",
                                font: ['Arial, Helvetica, sans-serif', [27, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"],
                                textStyle: ["", "", "25px", "", ""]
                            },
                            {
                                id: 't2',
                                type: 'text',
                                rect: ['177px', '372px', '521px', '87px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px; text-align: center;\">​<span style=\"color: rgb(255, 225, 31); font-size: 35px;\">¡Iniciemos el camino con la mejor disposición ¡</span></p><p style=\"margin: 0px;\">​</p>",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [27, "px"], "rgba(0,0,0,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "26px", "", "none"]
                            }]
                        },
                        {
                            id: 'stage3',
                            type: 'group',
                            rect: ['0px', '0px', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-ver',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-ver.jpg",'0px','0px']
                            },
                            {
                                id: 'img-3',
                                type: 'image',
                                rect: ['0px', '1px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"img-3.png",'0px','0px']
                            },
                            {
                                id: 'ico-3',
                                type: 'image',
                                rect: ['870px', '451px', '96px', '97px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"ico-3.png",'0px','0px']
                            },
                            {
                                id: 't4',
                                type: 'text',
                                rect: ['584px', '282px', '391px', '87px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"color: rgb(255, 225, 31); font-size: 35px;\"></span>Recuerde que posee únicamente dos intentos para solucionar este crucigrama. </p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\">Respetado estudiante pase el mouse sobre cada número para desplegar la definición de la palabra a registrar.</p>",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [20, "px"], "rgba(250,224,34,1.00)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "20px", "", "none"]
                            },
                            {
                                id: 't3',
                                type: 'text',
                                rect: ['585px', '110px', '366px', '189px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​Para iniciar el camino le invitamos a desarrollar este crucigrama, el cual le permitirá reforzar su apropiación de los términos asociados con el concepto de estilos de gerencia, liderazgo y su importancia&nbsp;</p><p style=\"margin: 0px;\">​</p>",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(255,255,255,1.00)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "24px", "", "none"]
                            },
                            {
                                id: 'app',
                                type: 'rect',
                                rect: ['78px', '129px', '395px', '419px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,0.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"]
                            },
                            {
                                id: 'Text4',
                                type: 'text',
                                rect: ['581px', '444px', '289px', '116px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px; line-height: 48px;\">​<span style=\"font-size: 63px;\">¡Muchos éxitos!</span></p><p style=\"margin: 0px;\">​</p>",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(255,255,255,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "57px", "", "none"]
                            }]
                        },
                        {
                            id: 'stage-fail',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-ama',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-ama.jpg",'0px','0px']
                            },
                            {
                                id: 'ico-4',
                                type: 'image',
                                rect: ['306px', '187px', '122px', '121px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"ico-4.png",'0px','0px']
                            },
                            {
                                id: 'Text5',
                                type: 'text',
                                rect: ['455px', '215px', '270px', '97px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 70px;\">¡Ánimo!&nbsp;</span></p>",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(255,255,255,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'Text6',
                                type: 'text',
                                rect: ['241px', '385px', '521px', '128px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​Con una mayor lectura y revisión del</p><p style=\"margin: 0px;\">material de apoyo, podremos mejorar nuestra conceptualización de los estilos  de gerencia , el liderazgo y su importancia</p>",
                                align: "center",
                                font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(255,255,255,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            }]
                        },
                        {
                            id: 'stage-good',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-ver2',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-ver.jpg",'0px','0px']
                            },
                            {
                                id: 'winner',
                                type: 'text',
                                rect: ['262px', '215px', '506px', '97px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​<span style=\"font-size: 70px;\">¡Felicitaciones!&nbsp;</span></p>",
                                align: "left",
                                font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(255,255,255,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            },
                            {
                                id: 'ico-6',
                                type: 'image',
                                rect: ['127px', '205px', '107px', '102px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"ico-6.png",'0px','0px']
                            },
                            {
                                id: 'ico-5',
                                type: 'image',
                                rect: ['763px', '200px', '107px', '107px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"ico-5.png",'0px','0px']
                            },
                            {
                                id: 'Text6Copy',
                                type: 'text',
                                rect: ['241px', '385px', '521px', '128px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​Ha logrado una apropiación del concepto de estilos de gerencia, el liderazgo y su importancia. Le invitamos a continuar con las siguientes actividades propuestas.</p><p style=\"margin:0px\">​</p>",
                                align: "center",
                                font: ['Arial, Helvetica, sans-serif', [22, "px"], "rgba(255,255,255,1)", "400", "none", "normal", "break-word", "normal"],
                                textStyle: ["", "", "", "", "none"]
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '270px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");
